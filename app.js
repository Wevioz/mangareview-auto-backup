const mongoose = require('mongoose');
const moment = require('moment');
const fs = require('fs');
const Json2csvParser = require("json2csv").Parser;

/**
 * Requiring .env file that contains environment variables.
 */
require('dotenv').config();

/**
 * Check if NODE_ENV env variable exists.
 */
if (!process.env.NODE_ENV) {
    console.error("[ENV] Variable d'environnement 'NODE_ENV' inexistante -- Exit Program");
    process.exit(1);
}

/**
 * Check if MONGO_DB env variable exists.
 */
 if (!process.env.MONGO_DB) {
    console.error("[ENV] Variable d'environnement 'MONGO_DB' inexistante -- Exit Program");
    process.exit(1);
}

/**
 * Check if MONGO_USER env variable exists.
 */
 if (!process.env.MONGO_USER) {
    console.error("[ENV] Variable d'environnement 'MONGO_USER' inexistante -- Exit Program");
    process.exit(1);
}

/**
 * Check if MONGO_PASSWORD env variable exists.
 */
if (!process.env.MONGO_PASSWORD) {
    console.error("[ENV] Variable d'environnement 'MONGO_PASSWORD' inexistante -- Exit Program");
    process.exit(1);
}

/**
 * Connection to Mongo Atlas Cloud.
 */
mongoose.connect(`mongodb+srv://Wevioz:${process.env.MONGO_PASSWORD}@cluster0.q9sshxs.mongodb.net/hitema?retryWrites=true&w=majority`)
 .then(() => { 
    console.log('[Mongo] Connected');

    
    mongoose.connection.db.listCollections().toArray(function (err, names){
        if (err) {
          console.log(err);
          throw err;
        } else {

            /**
             * Export collection to CSV format
             * @param {number} collection The current key in the list of collections.
             */
            async function Backup(collection) {
                // Mongo request to get collection data
                await names[collection]['data'].find({}).lean().exec((err, data) => {
                    if (err) {
                        console.log(`[BACKUP] Error, ${err}`);
                        throw err;
                    }

                    // Check if the collection have data
                    let header = (data.length == 0 ? [] : Object.keys(data[0]))
                    if(header.length == 0) return;

                    // Get the current date
                    let createdAt = moment().format('YYYYMMDDhhmmss');

                    // Create current date directory if not exist
                    let dir2 = `./backup/${createdAt}`;
                    if (!fs.existsSync(dir2)) fs.mkdirSync(dir2);

                    // Setting up CSVParser with header and data
                    const json2csvParser = new Json2csvParser(header);
                    const csvData = json2csvParser.parse(data);
                    
                    // Write all data to the desired CSV file and download it
                    const path = `${dir2}/${names[collection]['name']}.csv`;
                    fs.writeFile(path, csvData, (err) => {
                        if (err) {
                            console.log(`[BACKUP] Error, ${err}`);
                            throw err;
                        }
                        console.log(`[BACKUP] ${path} downloaded Successfully :)`);
                    }); 
                });
            }

            // Create Backup directory if not exist
            let dir = './backup';
            if (!fs.existsSync(dir)) fs.mkdirSync(dir);

            // Get the collections list
            for(collection in names){
                names[collection]['data'] = mongoose.model(names[collection]['name'], new mongoose.Schema());
                Backup(collection);
            }
        }
    });
 })
 .catch((err) => console.log("[Mongo] Not connected, Details : ", err));
